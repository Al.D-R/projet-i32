from modele import *
from database import db

class Reservation(Modele):
    """Les parametres de la reservation (dateSortie,heureSortie,nomBateau,numPlongeur*)"""

    def __init__(self, dateSortie, heureSortie, nomBateau, numPlongeur):
        self.dateSortie = dateSortie
        self.heureSortie = heureSortie
        self.nomBateau = nomBateau
        self.numPlongeur = numPlongeur
    def __str__(self):
        return  "dateSortie: " + str(self.dateSortie) + ", heureSortie" + str(self.heureSortie) + "nomBateau: " + self.nomBateau + ", numPlongeur" + str(self.numPlongeur)

    @staticmethod
    def get_all():
        #SELECT * FROM reservation;
        result = db.execute_and_fetch("SELECT * FROM reservation;")
        return list(map(Reservation.from_tuple, result))

    @staticmethod
    def get(dateSortie, heureSortie, nomBateau, numPlongeur):
        #SELECT * FROM reservation WHERE dateSortie=dateSortie;
        result = db.execute_and_fetch("SELECT * FROM reservation WHERE dateSortie = %s AND heureSortie = %s AND nomBateau = %s AND numPlongeur = %s;", (dateSortie, heureSortie, nomBateau, numPlongeur))[0]
        return Reservation.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO reservation (dateSortie, heureSortie, nomBateau, numPlongeur);
        #VALUES (self.dateSortie, self.heureSortie, self.nomBateau, self.numPlongeur);
        db.execute("INSERT INTO reservation (dateSortie, heureSortie, nomBateau, numPlongeur) VALUES (%s, %s, %s, %s);", (self.dateSortie, self.heureSortie, self.nomBateau, self.numPlongeur))
        db.commit()

    #def update(self):
    #    #UPDATE dateSortie
    #    #SET heureSortie=self.heureSortie, nomBateau=self.nomBateau, numPlongeur=self.numPlongeur
    #    #WHERE dateSortie=self.dateSortie;
    #    db.execute("""UPDATE reservation
    #    SET heureSortie = %s, nomBateau = %s, numPlongeur = %s
    #    WHERE dateSortie = %s;""", (self.dateSortie, self.heureSortie, self.nomBateau, self.numPlongeur))
    #    db.commit()

    def remove(self):
        #DELETE FROM reservation WHERE dateSortie = self.dateSortie;
        db.execute("DELETE FROM reservation WHERE dateSortie = %s AND heureSortie = %s AND nomBateau = %s AND numPlongeur = %s;",
                   (self.dateSortie, self.heureSortie, self.nomBateau, self.numPlongeur))
        db.commit()


    @staticmethod
    def from_tuple(t):
        if len(t) != 4: raise TupleException("Le tuple ne contient pas autant de donnÃ©es que la class reservation")
        l = list(t)
        m = reservation(l[0], l[1], l[2], l[3])
        return m
