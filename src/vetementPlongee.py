from modele import *

class VetementPlongee(Modele):
    """la classe des vetement de Plongee (nomVetement, taille, prixVetement, quantite)"""

    def __init__(self, nomVetement, taille, prixVetement, quantite):
        self.nomVetement = nomVetement
        self.taille = taille
        self.prixVetement = prixVetement
        self.quantite = quantite
    def __str__(self):
        return "Vetement: " + str(self.nomVetement) + ", taille: " + str(self.taille) +", prix: "+str(self.prixVetement)+", quantite: "+str(self.quantite)

    @staticmethod
    def get_all():
        #SELECT * FROM vetementPlongee;
        result = db.execute_and_fetch("SELECT * FROM vetementPlongee;")
        return list(map(vetementPlongee.from_tuple, result))

    @staticmethod
    def get(nomVetement,taille):
        #SELECT * FROM vetementPlongee WHERE nomVetement=nomVetement AND taille=taille;
        result = db.execute_and_fetch("SELECT * FROM vetementPlongee WHERE nomVetement = %s AND taille = %s;", (nomVetement,taille))[0]
        return vetementPlongee.from_tuple(result) # On transforme un tuple en vetement de plongee

    def insert(self):
        #INSERT INTO vetementPlongee  (nomVetement, taille, prixVetement, quantite)
        #VALUES (self.nomVetement, self.taille, self.prixVetement, self.quantite );
        db.execute("INSERT INTO vetementPlongee (nomVetement, taille, prixVetement, quantite) VALUES (%s, %s, %s, %s);", (self.nomVetement, self.taille, self.prixVetement, self.quantite))
        db.commit()

    def update(self):
        #UPDATE vetementPlongee
        #SET prixVetement=self.prixVetement, quantite = self.quantite
        #WHERE nomVetement = self.nomVetement AND taille = self.taille;
        db.execute("""UPDATE vetementPlongee
        SET prixVetement = %s, quantite = %s
        WHERE nomVetement = %s AND taille = %s ;""", (self.prixVetement, self.quantite, self.nomVetement, self.taille))
        db.commit()


    def remove(self):
        #DELETE FROM vetementPlongee WHERE nomVetement = self.nomVetement AND taille = self.taille;
        db.execute("DELETE FROM vetement WHERE nomVetement = %s AND taille = %s;", (self.nomVetement,self.taille,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 4: raise TupleException("Le tuple ne contient pas autant de données que la class Vetement")
        l = list(t)
        v = vetementPlongee(l[0], l[1], l[2], l[3])
        return v

