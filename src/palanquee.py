from modele import *

class Palanquee(Modele):
    """la classe des sites de Plongee (numPalanquee, moniteur)"""

    def __init__(self, numPalanquee, moniteur):
        self.numPalanquee = numPalanquee
        self.moniteur = moniteur
    def __str__(self):
        return "palanquee: " + str(self.numPalanquee) + ", moniteur: " + str(self.moniteur)

    @staticmethod
    def get_all():
        result = db.execute_and_fetch("SELECT * FROM palanquee;")
        return list(map(Palanquee.from_tuple, result))

    @staticmethod
    def get(numPalanquee):
        result = db.execute_and_fetch("SELECT * FROM palanquee WHERE numPalanquee = %s;", (numPalanquee,))[0] # On extrait le seul element de la liste
        return Palanquee.from_tuple(result) # On transforme un tuple en Palanquee

    def insert(self):
        db.execute("INSERT INTO palanquee (numPalanquee, moniteur) VALUES (%s, %s);", (self.numPalanquee, self.moniteur))
        db.commit()

    def update(self):
        db.execute("""UPDATE palanquee
        SET moniteur = %s
        WHERE numPalanquee = %s;""", (self.numPalanquee, self.moniteur))
        db.commit()

    def remove(self):
        db.execute("DELETE FROM palanquee WHERE numPalanquee = %s;", (self.numPalanquee,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 2: raise TupleException("Le tuple ne contient pas autant de données que la class Palanquee")
        l = list(t)
        p = Palanquee(l[0], l[1])
        return p
