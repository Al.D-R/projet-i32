from modele import *
from database import db

class Plongeur(Modele):
    """Un plongeur"""
    def __init__(self, nom, prenom, dateNaissance, email, numTelephone, adresse, dateCertificatMedical, resteAPayer, dateLimite, numNiveau, organisme, numPalanquee):
        self.nom = nom
        self.prenom = prenom
        self.dateNaissance = dateNaissance
        self.email = email
        self.numTelephone = numTelephone
        self.adresse = adresse
        self.dateCertificatMedical = dateCertificatMedical
        self.resteAPayer = resteAPayer
        self.dateLimite = dateLimite
        self.numNiveau = numNiveau
        self.organisme = organisme
        self.numPalanquee = numPalanquee
    def __str__(self):
        return "nom: " + str(self.nom) + ", prenom: " + str(self.prenom) +", date de Naissance: "+str(self.dateNaissance)+", email: "+str(self.email)+", numero Telephone: "+str(self.numTelephone)+", adresse: "+str(self.adresse)+", date Certificat Medical: "+str(self.dateCertificatMedical)+", reste Ã  Payer: "+str(self.resteAPayer)+", date Limite: "+str(self.dateLimite)

    @staticmethod
    def get_all():
        result = db.execute_and_fetch("SELECT * FROM Plongeur;")
        return list(map(Plongeur.from_tuple, result))

    @staticmethod
    def get(numPlongeur):
        result = db.execute_and_fetch("SELECT * FROM Plongeur WHERE numPlongeur = %s;", (numPlongeur))[0]
        return Plongeur.from_tuple(result) # On transforme un tuple en vetement de plongee

    def insert(self):
        db.execute("INSERT INTO Plongeur (nom, prenom, dateNaissance, email, numTelephone, adresse, dateCertificatMedical, resteAPayer, dateLimite, numNiveau, organisme, numPalanquee) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);", (self.nom, self.prenom, self.dateNaissance, self.email, self.numTelephone, self.adresse, self.dateCertificatMedical, self.resteAPayer, self.dateLimite, self.numNiveau, self.organisme, self.numPalanquee))
        db.commit()

    def update(self):
        db.execute("""UPDATE Plongeur
        SET nom =%s, prenom =%s, dateNaissance =%s, email =%s, numTelephone =%s, adresse =%s, dateCertificatMedical =%s, resteAPayer =%s, dateLimite =%s
        WHERE numPlongeur = %s;""", (self.nom, self.prenom, self.dateNaissance, self.email, self.numTelephone, self.adresse, self.dateCertificatMedical, self.resteAPayer, self.dateLimite, self.numPlongeur))
        db.commit()


    def remove(self):
        db.execute("DELETE FROM Plongeur WHERE numPlongeur = %s;", (self.numPlongeur))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 13: raise TupleException("Le tuple ne contient pas autant de données que la class Plongeur")
        l = list(t)
        p = Plongeur(l[1], l[2], l[3],l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11], l[12])
        return p
