from modele import *
from database import db

class Sortie(Modele):
    """Les parametres de la sortie (dateSortie,heureSortie,nomBateau,site)"""

    def __init__(self, dateSortie, heureSortie, nomBateau, site):
        self.dateSortie = dateSortie
        self.heureSortie = heureSortie
        self.nomBateau = nomBateau
        self.site = site
    def __str__(self):
        return  "dateSortie: " + str(self.dateSortie) + ", heureSortie: " + str(self.heureSortie) + " nomBateau: " + self.nomBateau + ", site" + str(self.site)


    @staticmethod
    def get_all():
        #SELECT * FROM sortie;
        result = db.execute_and_fetch("SELECT * FROM sortie;")
        return list(map(Sortie.from_tuple, result))

    @staticmethod
    def get(dateSortie, heureSortie, nomBateau):
        #SELECT * FROM sortie WHERE dateSortie=dateSortie;
        result = db.execute_and_fetch("SELECT * FROM sortie WHERE dateSortie = %s AND heureSortie = %s AND nomBateau = %s ;", (dateSortie,heureSortie,nomBateau))[0]
        return Sortie.from_tuple(result) # On transforme un tuple en matos


    def insert(self):
        #INSERT INTO sortie (dateSortie, heureSortie, nomBateau, site);
        #VALUES (self.dateSortie, self.heureSortie, self.nomBateau, self.site);
        db.execute("INSERT INTO sortie (dateSortie, heureSortie, nomBateau, site) VALUES (%s, %s, %s, %s);", (self.dateSortie, self.heureSortie, self.nomBateau, self.site))
        db.commit()

    def update(self):
        #UPDATE dateSortie
        #SET heureSortie=self.heureSortie, nomBateau=self.nomBateau, site=self.site
        #WHERE dateSortie=self.dateSortie;
        db.execute("""UPDATE sortie
        SET heureSortie = %s, nomBateau = %s, site = %s
        WHERE dateSortie = %s;""", (self.dateSortie, self.heureSortie, self.nomBateau, self.site))
        db.commit()

    def remove(self):
        #DELETE FROM sortie WHERE dateSortie = self.dateSortie;
        db.execute("DELETE FROM sortie WHERE dateSortie = %s AND heureSortie = %s AND nomBateau = %s;", (self.dateSortie, self.heureSortie, self.nomBateau))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 4: raise TupleException("Le tuple ne contient pas autant de donnÃ©es que la class sortie")
        l = list(t)
        m = Sortie(l[0], l[1], l[2], l[3])
        return m

    def add_monitor(self, m):
        db.execute(""" INSERT INTO presenceMoniteurSortie (dateSortie, heureSortie, nomBateau, numMoniteur)
        VALUES (%s,%s,%s,%s); """, (self.dateSortie,self.heureSortie,self.nomBateau, m.numMoniteur))
        db.commit()

    def remove_monitor(self,m):
        db.execute(""" DELETE FROM presenceMoniteurSortie WHERE numMoniteur = %s;""" , (m.numMoniteur))
        db.commit()
