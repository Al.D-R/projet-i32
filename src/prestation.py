from modele import *

class Prestation(Modele):
    """Prestation (formation/exploration)"""

    def __init__(self, typePrestation, prix):
        self.typePrestation = typePrestation
        self.prix = prix
    def __str__(self):
        return "Type: " + self.typePrestation + ", prix: " + str(self.prix)

    @staticmethod
    def get_all():
        #SELECT * FROM Prestation;
        result = db.execute_and_fetch("SELECT * FROM prestation;")
        return list(map(Prestation.from_tuple, result))

    @staticmethod
    def get(typePrestation):
        #SELECT * FROM Prestation WHERE typePrestation=typePrestation;
        result = db.execute_and_fetch("SELECT * FROM prestation WHERE typePrestation = %s;", (typePrestation))[0]
        return Prestation.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO Prestation  (typePrestation, prix)
        #VALUES (self.typePrestation, self.prix);
        db.execute("INSERT INTO prestation (typePrestation, prix) VALUES (%s, %s);", (self.typePrestation, self.prix))
        db.commit()

    def update(self):
        #UPDATE Prestation
        #SET prix=self.prix
        #WHERE typePrestation = self.typePrestation;
        db.execute("""UPDATE prestation
        SET prix = %s
        WHERE typePrestation = %s;""", (self.prix))
        db.commit()

    def remove(self):
        #DELETE FROM Prestation WHERE typePrestation = self.typePrestation;
        db.execute("DELETE FROM prestation WHERE typePrestation = %s;", (self.typePrestation))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 2: raise TupleException("Le tuple ne contient pas autant de donnÃ©es que la class Prestation")
        l = list(t)
        m = Prestation(l[0], l[1])
        return m

