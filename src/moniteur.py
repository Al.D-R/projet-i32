from modele import *
from database import db

class Moniteur(Modele):
    """Les stats du moniteur"""

    def __init__(self, nom, prenom):
        self.nom = nom
        self.prenom = prenom
    def __str__(self):
        return  "numMoniteur: " + str(self.numMoniteur) + ", nom" + self.nom + "prenom: " + self.prenom

    @staticmethod
    def get_all():
        result = db.execute_and_fetch("SELECT * FROM moniteur;")
        return list(map(Moniteur.from_tuple, result))

    @staticmethod
    def get(numMoniteur):
        result = db.execute_and_fetch("SELECT * FROM Moniteur WHERE numMoniteur = %s;", (numMoniteur,))[0] # On extrait le seul element de la liste
        return Moniteur.from_tuple(result) # On transforme un tuple en Moniteur

    def insert(self):
        db.execute("INSERT INTO Moniteur (nom, prenom) VALUES (%s, %s);", (self.nom, self.prenom))
        db.commit()

    def update(self):
        db.execute("""UPDATE Moniteur
        SET nom = %s, prenom = %s
        WHERE numMoniteur = %s;""", (self.numMoniteur, self.nom, self.prenom))
        db.commit()

    def remove(self):
        db.execute("DELETE FROM Moniteur WHERE numMoniteur = %s;", (self.numMoniteur,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 3: raise TupleException("Le tuple ne contient pas autant de données que la class Palanquee")
        l = list(t)
        m = Palanquee(l[0], l[1], l[2])
        return m
