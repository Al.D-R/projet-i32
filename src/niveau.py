from modele import *

class Niveau(Modele):
    """La classe niveaux des plongeurs (numNiveau,organisme,profondeurPE,profondeurPA)"""

    def __init__(self, numNiveau, organisme, profondeurPE, profondeurPA):
        self.numNiveau = numNiveau
        self.organisme = organisme
        self.profondeurPE = profondeurPE
        self.profondeurPA = profondeurPA
    def __str__(self):
        return "Niveau: " + str(self.numNiveau) + ", organisme: " + str(self.organisme) +", profondeurPE: "+str(self.organisme)+", profondeurPA: "+str(self.profondeurPA)

    @staticmethod
    def get_all():
        #SELECT * FROM niveau
        result = db.execute_and_fetch("SELECT * FROM niveau;")
        return list(map(Niveau.from_tuple, result))

    @staticmethod
    def get(numNiveau, organisme):
        #SELECT * FROM niveau WHERE numNiveau = numNiveau AND organisme = organisme
        result = db.execute_and_fetch("SELECT * FROM niveau WHERE numNiveau = %s AND organisme = %s;", (numNiveau,organisme))[0]
        return Niveau.from_tuple(result) # On transforme un tuple en niveau (parlant de niveau, vous connaissez le point commun entre un niveau et ma moyenne ?.... la bulle, hahaha, la bulle, c'est rigolo...)

    @staticmethod
    def from_tuple(t):
        if len(t) != 4: raise TupleException("le tuple ne contient pas autant de donnees que la class Niveau")
        l = list(t)
        n = Niveau(l[0], l[1], l[2], l[3], )
        return n

