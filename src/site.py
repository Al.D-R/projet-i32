from modele import *

class Site(Modele):
    """la classe des sites de Plongee (nomSite, typeSite, coordonnees, supplement, prerogativeMinPE)"""

    def __init__(self, nomSite, typeSite, coordonnees, supplement, prerogativeMinPE):
        self.nomSite = nomSite
        self.typeSite = typeSite
        self.coordonnees = coordonnees
        self.supplement = supplement
        self.prerogativeMinPE = prerogativeMinPE
    def __str__(self):
        return "Site: " + str(self.nomSite) + ", type: " + str(self.typeSite) +", coordonnees: "+str(self.coordonnees)+", supplement: "+str(self.supplement)+", prerogativeMinPE: "+str(self.prerogativeMinPE)


    @staticmethod
    def get_all():
        #SELECT * FROM site;
        result = db.execute_and_fetch("SELECT * FROM site;")
        return list(map(site.from_tuple, result))

    @staticmethod
    def get(nomSite):
        #SELECT * FROM site WHERE nomSite=nomSite;
        result = db.execute_and_fetch("SELECT * FROM site WHERE nomSite = %s;", (nomSite))[0]
        return site.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO site  (nomSite, typeSite, coordonnees, supplement, prerogativeMinPE)
        #VALUES (self.nomSite, self.typeSite, self.coordonnees, self.supplement, self.prerogativeMinPE);
        db.execute("INSERT INTO site (nomSite, typeSite, coordonnees, supplement, prerogativeMinPE) VALUES (%s, %s, %s, %s, %s);", (self.nomSite, self.typeSite, self.coordonnees, self.supplement, self.prerogativeMinPE))
        db.commit()

    def update(self):
        #UPDATE site
        #SET typeSite=self.typeSite, coordonnees = self.coordonnees, supplement = self.supplement, prerogativeMinPE = self.prerogativeMinPE
        #WHERE nomSite = self.nomSite;
        db.execute("""UPDATE site
        SET typeSite = %s, coordonnees = %s, supplement = %s, prerogativeMinPE = %s
        WHERE nomSite = %s;""", (self.typeSite, self.coordonnees, self.supplement, self.prerogativeMinPE, self.nomSite))
        db.commit()

    def remove(self):
        #DELETE FROM site WHERE nomSite = self.nomSite;
        db.execute("DELETE FROM site WHERE nomSite = %s;", (self.nomSite))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 5: raise TupleException("Le tuple ne contient pas autant de donnÃ©es que la class site")
        l = list(t)
        m = site(l[0], l[1], l[2], l[3], l[4])
        return m
