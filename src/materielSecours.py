from modele import *
from database import db

class MaterielSecours(Modele):
    """la classe du materiel de Secours obligatoire (nomBateau, nomMateriel, quantite)"""

    def __init__(self, nomBateau, nomMateriel, quantite):
        self.nomBateau= nomBateau
        self.nomMaterielSecours = nomMateriel
        self.quantite = quantite
    def __str__(self):
        return "Materiel de secours: " + self.nomMaterielSecours + ", Bateau: " + self.nomBateau + ", quantite: " + str(self.quantite)

    @staticmethod
    def get_all():
        #SELECT * FROM materielSecours;
        result = db.execute_and_fetch("SELECT * FROM materielSecours;")
        return list(map(MaterielSecours.from_tuple, result))

    @staticmethod
    def get(nomBateau, nomMaterielSecours):
        #SELECT * FROM materielSecours WHERE nomMateriel=nomMateriel AND taille=taille;
        result = db.execute_and_fetch("SELECT * FROM materielSecours WHERE nomBateau = %s AND nomMaterielSecours = %s;", (nomBateau,nomMaterielSecours))[0]
        return MaterielSecours.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO materielSecours (nomMateriel, taille, prixMateriel, quantite);
        #VALUES (self.nomMateriel, self.taille, self.prixMateriel, self.quantite);
        db.execute("INSERT INTO materielSecours (nomBateau, nomMaterielSecours quantite) VALUES (%s, %s, %s);", (self.nomBateau, self.nomMaterielSecours, self.quantite))
        db.commit()

    def update(self):
        #UPDATE nomMateriel SET prixMateriel=self.prixMateriel,quantite=self.quantite WHERE nomMateriel=self.nomMateriel AND taille=self.taille;
        db.execute("""UPDATE materielSecours
        SET quantite = %s
        WHERE nomBateau = %s AND nomMaterielSecours = %s ;""", (self.quantite, self.nomBateau, self.nomMaterielSecours))
        db.commit()

    def remove(self):
        #DELETE FROM materielSecours WHERE nomMateriel = self.nomMateriel AND taille = self.taille;
        db.execute("DELETE FROM materielSecours WHERE nomBateau = %s AND nomMaterielSecours = %s;", (self.nomBateau,self.nomMaterielSecours))
        db.commit()


    @staticmethod
    def from_tuple(t):
        if len(t) != 3: raise TupleException("Le tuple ne contient pas autant de donnees que la class materielSecours")
        l = list(t)
        m = MaterielSecours(l[0], l[1], l[2])
        return m
