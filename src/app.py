from datetime import *
from bateau import *
from plongeur import *
from bouteille import *
from moniteur import *
from materiel import *
from materielSecours import *
from reservation import *
from sortie import *
from database import db

class App(object):
    """L'application"""

    def __init__(self):
        pass

    def executer(self):
        print("[0]: Quitter")
        print("[1]: Gerer les reservations")
        print("[2]: Gerer les navires")
        print("[3]: Gerer les sorties")
        print("[4]: Gerer le materiel")
        print("[5]: Gerer le materiel de secours")
        print("[6]: Gerer les bouteilles")
        print("[7]: Afficher les plongeurs")
        print("[8]: Enregistrer un plongeur")
        print("[9]: Enregistrer un moniteur")

        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.gerer_reservations()
        elif choice == 2: self.gerer_bateaux()
        elif choice == 3: self.gerer_sorties()
        elif choice == 4: self.gerer_materiel()
        elif choice == 5: self.gerer_materiel_secours()
        elif choice == 6: self.gerer_bouteilles()
        elif choice == 7: self.afficher_plongeurs()
        elif choice == 8: self.enregistrer_plongeur()
        elif choice == 9: self.enregistrer_moniteur()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

        db.close()

    def gerer_reservations(self):
        print("[0]: Quitter")
        print("[1]: Afficher les reservations")
        print("[2]: Ajouter une reservation")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_reservations()
        elif choice == 2: self.ajouter_reservation()

    def gerer_bateaux(self):
        print("[0]: Quitter")
        print("[1]: Afficher les navires")
        print("[2]: Modifier la disponibilite d'un navire")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_bateaux()
        elif choice == 2: self.modifier_disponibilite_bateau()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

    def gerer_palanquees(self):
        pass

    def gerer_sorties(self):
        print("[0]: Quitter")
        print("[1]: Afficher les sorties")
        print("[2]: Ajouter une sortie")
        print("[3]: Supprimer une sortie")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_sorties()
        elif choice == 2: self.ajouter_sortie()
        elif choice == 3: self.supprimer_sortie()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

    def gerer_materiel(self):
        print("[0]: Quitter")
        print("[1]: Afficher le materiel")
        print("[2]: Modifier une quantite de materiel")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_materiel()
        elif choice == 2: self.modifier_quantite_materiel()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

    def modifier_quantite_materiel(self):
        nomMateriel = input("Nom du materiel: ")
        taille = int(input("Taille: "))
        quantite = int(input("Selectionnez la quantite a ajouter (peut etre negative): "))
        m = Materiel.get(nomMateriel, taille)
        m.quantite += quantite
        m.update()

    def gerer_materiel_secours(self):
        print("[0]: Quitter")
        print("[1]: Afficher le materiel de secours")
        print("[2]: Modifier une quantite de materiel de secours")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_materiel_secours()
        elif choice == 2: self.modifier_quantite_materiel_secours()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

    def afficher_materiel_secours(self):
        l = MaterielSecours.get_all()
        for m in l:
            print(m)

    def modifier_quantite_materiel_secours(self):
        nomMateriel = input("Nom du materiel de secours: ")
        nomBateau = input("Nom du bateau: ")
        quantite = int(input("Selectionnez la quantite a ajouter (peut etre negative): "))
        m = MaterielSecours.get(nomBateau, nomMateriel)
        m.quantite += quantite
        m.update()

    def gerer_bouteilles(self):
        print("[0]: Quitter")
        print("[1]: Afficher les bouteilles")
        choice = int(input("Choisissez: "))
        if choice == 0: exit(0)
        elif choice == 1: self.afficher_bouteilles()
        else:
            print("Veuillez choisir une commande existante")
            exit(1)

    def afficher_bouteilles(self):
        l = Bouteille.get_all()
        for b in l:
            print(b)


    def enregistrer_moniteur(self):
        nom = input("Nom: ")
        prenom = input("Prenom: ")
        m = Moniteur(nom, prenom)
        m.insert()

    def afficher_reservations(self):
        # recuperer les reservation; les afficher
        l = Reservation.get_all()
        for r in l:
            print(r)

    def ajouter_reservation(self):
        # Recuperer les attributs de l'utilisateur; enregistrer la reservation
        jour = int(input("Jour de reservation: "))
        mois = int(input("Mois de reservation: "))
        annee = int(input("Annee de reservation: "))
        d = date(annee, mois, jour)
        r = Reservation()

    def afficher_materiel(self):
        # Recuperer materiel; filtrer estDisponible = true; les afficher
        l = Materiel.get_all()
        for m in l:
            print(m)

    def afficher_plongeurs(self):
        # Recuperer les plongeurs; les afficher
        l = Plongeur.get_all()
        for p in l:
            print(p)

    def afficher_bateaux(self):
        # Recuperer les bateux; filtrer estDisponible = true; les afficher
        l = Bateau.get_all()
        for b in l:
            print(b)

    def modifier_disponibilite_bateau(self):
        nomBateau = input("Choisissez le nom du beateu: ")
        b = Bateau.get(nomBateau)
        b.estDisponible = not(b.estDisponible)
        b.update()

    def afficher_palanquees(self):
        # Recuperer les palanquees; les afficher
        pass

    def ajouter_palanquee(self):
        # Recuperer les attributs de l'utilisateur; creer une palanquee; l'enregistrer
        pass

    def supprimmer_palanquee(self):
        # Recuperer numPalanquee de l'utilisateur; supprimmer la palanquee
        pass

    def afficher_sorties(self):
        l = Sortie.get_all()
        for s in l:
            print(s)


    def ajouter_sortie(self):
        # Recuperer attributs; creer sortie; l'enregistrer
        print("Date de Sortie (uniquement en nombres) : ")
        day = int(input("Entrer le jour:"))
        month = int(input('Entrer le mois: '))
        year = int(input("Entrer l'année: "))
        dateSortie = date(year, month, day)
        hour=int(input("Heure: "))
        minute=int(input("Minutes: "))
        heureSortie = time(hour,minute)
        nomBateau = input("Nom du bateau: ")
        site = input("Le site: ")
        S = Sortie(dateSortie,heureSortie,nomBateau,site)
        S.insert()

    def supprimer_sortie(self):
        # Recuperer dateSortie; heureSortie; bateau; supprimmer la sortie
        print("Date de Sortie (uniquement en nombres) : ")
        day = int(input("Entrer le jour:"))
        month = int(input('Entrer le mois: '))
        year = int(input("Entrer l'année: "))
        dateSortie = date(year, month, day)
        hour=int(input("Heure: "))
        minute=int(input("Minutes: "))
        heureSortie = time(hour,minute)
        nomBateau = input("Nom du bateau: ")
        S = Sortie.get(dateSortie,heureSortie,nomBateau)
        S.remove()

    def enregistrer_plongeur(self):
        nom = input("Nom: ")
        prenom = input("Prenom: ")
        jour = int(input("Jour de naissance: "))
        mois = int(input("Mois de naissance: "))
        annee = int(input("Annee de naissance: "))
        dateNaissance = date(annee, mois, jour)
        adresse = input("Adresse: ")
        mail = input("Adresse email: ")
        tel = int(input("Numero de telephone: "))
        jourCert = int(input("Jour du certificat: "))
        moisCert = int(input("Mois du certificat: "))
        anneeCert = int(input("Annee du certificat: "))
        dateCert = date(anneeCert, moisCert, jourCert)
        numNiveau = input("Numero de niveau: ")
        organisme = input("Organisme: ")
        numPalanquee = int(input("Numero de palanquee"))
        p = Plongeur(nom, prenom, dateNaissance, mail, tel, adresse, dateCert, 0, date.today(), numNiveau, organisme, numPalanquee)
        p.insert()

app = App()
app.executer()
