from modele import *
from database import db

class Bouteille(Modele):
    """Les bouteilles de gaz"""

    def __init__(self, idBouteille, volume, materiaux, configuration, gaz):
        self.idBouteille = idBouteille
        self.volume = volume
        self.materiaux = materiaux
        self.configuration = configuration
        self.gaz = gaz
    def __str__(self):
        return "Id: " + str(self.idBouteille) +  ", volume: " + str(self.volume) + ", materiaux: " + str(self.materiaux) +", configuration: "+str(self.configuration)+", gaz: "+str(self.gaz)


    @staticmethod
    def get_all():
        #SELECT * FROM Bouteille;
        result = db.execute_and_fetch("SELECT * FROM bouteille;")
        return list(map(Bouteille.from_tuple, result))


    @staticmethod
    def get(volume, materiaux, configuration):
        #SELECT * FROM Bouteille WHERE volume=volume AND materiaux=materiaux AND configuration=configuration;
        result = db.execute_and_fetch("SELECT * FROM bouteille WHERE volume = %s AND materiaux = %s AND configuration = %s;", (volume,materiaux,configuration))[0]
        return Bouteille.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO Bouteille (volume, materiaux, configuration, quantite, quantiteDispo, prix);
        #VALUES (self.volume, self.materiaux, self.configuration, self.quantite, self.quantiteDispo, self.prix);
        db.execute("INSERT INTO bouteille (volume, materiaux, configuration, quantite, quantiteDispo, prix) VALUES (%s, %s, %s, %s, %s, %s);", (self.volume, self.materiaux, self.configuration, self.quantite, self.quantiteDispo, self.prix))
        db.commit()

    def update(self):
        #UPDATE volume, materiaux, configuration
        #SET prix=self.prix, quantite=self.quantite, quantiteDispo=self.quantiteDispo;
        #WHERE volume=self.volume AND materiaux=self.materiaux AND configuration=self.configuration;
        db.execute("""UPDATE bouteille
        SET prix = %s, quantite = %s, quantiteDispo = %s,
        WHERE volume = %s AND materiaux = %s AND configuration = %s;""", (self.volume, self.materiaux, self.configuration, self.quantite, self.quantiteDispo, self.prix))
        db.commit()

    def remove(self):
        #DELETE FROM Bouteille WHERE volume=self.volume AND materiaux=self.materiaux AND configuration=self.configuration;
        db.execute("DELETE FROM bouteille WHERE volume = %s AND materiaux = %s AND configuration = %s;", (self.volume, self.materiaux, self.configuration,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 5: raise TupleException("Le tuple ne contient pas autant de donnÃ©es que la class Bouteille")
        l = list(t)
        b = Bouteille(l[0], l[1], l[2], l[3], l[4])
        return b
